import { Component, OnInit } from "@angular/core";
import { ProductosService } from "src/app/services/productos.service";
import { Producto } from "src/app/models/producto.model";

@Component({
  selector: "app-catalogo",
  templateUrl: "./catalogo.component.html",
  styleUrls: ["./catalogo.component.css"],
  providers: [ProductosService]
})
export class CatalogoComponent implements OnInit {
  productos: Producto[];
  isLoaded: boolean;

  constructor(private productosService: ProductosService) {}

  ngOnInit() {
    this.productosService.getProductos().subscribe((productos) => {
      this.productos = productos;
      setTimeout(() => {
        this.isLoaded = true;
      }, 2000);
    });
  }
}
