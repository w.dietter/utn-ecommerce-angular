import { Component, OnInit } from "@angular/core";
import { ProductosService } from "src/app/services/productos.service";
import { Producto } from "src/app/models/producto.model";

@Component({
  selector: "app-altaproducto",
  templateUrl: "./altaproducto.component.html",
  styleUrls: ["./altaproducto.component.css"]
})
export class AltaProductoComponent implements OnInit {
  producto: Producto;
  nombre: string = "";
  precio: number = 0;
  descripcion: string = "";
  imgUrl: string = "";

  onSubmit(e) {
    e.preventDefault();
    this.producto = new Producto(
      this.nombre,
      this.precio,
      this.descripcion,
      this.imgUrl
    );
    console.log(this.precio);
    this.addProducto(this.producto);
  }

  constructor(private productosService: ProductosService) {}

  addProducto(producto: Producto) {
    console.log(producto);
    this.productosService.addProducto(producto);
  }
  ngOnInit() {}
}
