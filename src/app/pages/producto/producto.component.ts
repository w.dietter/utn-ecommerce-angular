import { Component, OnInit } from "@angular/core";
import { ProductosService } from "src/app/services/productos.service";
import { ActivatedRoute } from "@angular/router";
import { Producto } from "src/app/models/producto.model";

@Component({
  selector: "app-producto",
  templateUrl: "./producto.component.html",
  styleUrls: ["./producto.component.css"],
  providers: [ProductosService]
})
export class ProductoComponent implements OnInit {
  id: string;
  producto: Producto;
  isLoaded: boolean = false; 
  constructor(
    private productoService: ProductosService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      console.log(typeof params["id"]);
      this.id = params["id"].toString();
    });
    this.getProducto(this.id);
  }

  getProducto(id: string) {
    this.productoService.getProducto(id).subscribe((p) => {
      const producto = p[0];
      this.producto = new Producto(
        producto["nombre"],
        producto["precio"],
        producto["descripcion"],
        producto["imgUrl"]
      );
      setTimeout(() => {
        this.isLoaded = true;
      }, 2000);
    });
  }
}
