import { Component, OnInit } from "@angular/core";
import { UsuariosService } from "src/app/services/usuarios.service";
import { Usuario } from "src/app/models/usuario.model";

@Component({
  selector: "app-usuarios",
  templateUrl: "./usuarios.component.html",
  styleUrls: ["./usuarios.component.css"]
})
export class UsuariosComponent implements OnInit {
  usuarios: Usuario[];
 

  constructor(private usuariosService: UsuariosService) {}

  ngOnInit() {
    this.getUsuarios();
  }

  

  getUsuarios() {
    this.usuariosService.getUsuarios().subscribe((usuarios) => {
      console.log(usuarios);
      this.usuarios = usuarios;
    });
  }

  
}
