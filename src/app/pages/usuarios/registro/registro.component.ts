import { Component, OnInit } from "@angular/core";
import { Usuario } from 'src/app/models/usuario.model';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: "app-registro",
  templateUrl: "./registro.component.html",
  styleUrls: ["./registro.component.css"]
})
export class RegistroComponent implements OnInit {
  nombre: string = "";
  apellido: string = "";
  email: string = "";
  password: string = "";

  constructor(private usuariosService: UsuariosService) {}

  ngOnInit() {}
  
  onSubmit(event) {
    event.preventDefault();
    const usuario = new Usuario(
      this.nombre,
      this.apellido,
      this.email,
      this.password
    );

    this.addUsuario(usuario);
  }
  addUsuario(usuario: Usuario) {
    this.usuariosService
      .addUsuario(usuario)
      .subscribe((data) => console.log(data));
  }
}
