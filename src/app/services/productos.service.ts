import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Producto } from "../models/producto.model";

@Injectable({
  providedIn: "root"
})
export class ProductosService {
  constructor(private http: HttpClient) {}

  getProductos() {
    return this.http.get<Producto[]>("http://localhost:3000/productos");
  }

  getProducto(id: string) {
    return this.http.get<Producto>(`http://localhost:3000/productos/${id}`);
  }

  addProducto(producto: Producto) {
    console.log("On ProductoService addProducto()")
    console.log(producto);
    let headers: HttpHeaders = new HttpHeaders().set(
      "Content-Type",
      "application/json"
    );
    this.http.post(
      "http://localhost:3000/productos",
      {
        nombre: producto['nombre'],
        precio: producto['precio'],
        descripcion: producto['descripcion'],
        imgUrl: producto['imgUrl']
      },
      { headers }
    ).subscribe(data => console.log(data));
  }
}

