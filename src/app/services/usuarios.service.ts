import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Usuario } from "../models/usuario.model";

@Injectable({
  providedIn: "root"
})
export class UsuariosService {
  constructor(private http: HttpClient) {}

  getUsuarios() {
    return this.http.get<Usuario[]>(`http://localhost:3000/usuarios`);
  }

  getUsuario(id: string) {}

  addUsuario(usuario: Usuario) {
    let headers: HttpHeaders = new HttpHeaders().set(
      "Content-Type",
      "application/json"
    );
    return this.http.post(
      "http://localhost:3000/usuarios",
      {
        nombre: usuario.nombre,
        apellido: usuario.apellido,
        email: usuario.email,
        password: usuario.password
      },
      { headers }
    );
  }
}
