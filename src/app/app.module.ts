import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { HomeComponent } from "./pages/home/home.component";
import { CatalogoComponent } from "./pages/catalogo/catalogo.component";
import { RegistroComponent } from "./pages/usuarios/registro/registro.component";
import { HttpClientModule } from "@angular/common/http";
import { ProductoComponent } from './pages/producto/producto.component';
import { LoaderComponent } from './loader/loader.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AltaProductoComponent } from './pages/admin/altaproducto/altaproducto.component';
import { UsuariosComponent } from './pages/usuarios/usuarios/usuarios.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CatalogoComponent,
    RegistroComponent,
    ProductoComponent,
    LoaderComponent,
    AdminComponent,
    AltaProductoComponent,
    UsuariosComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
