export class Producto {
  id: number;
  nombre: string;
  precio: number;
  descripcion: string;
  imgUrl: string;

  constructor(
    nombre: string,
    precio: number,
    descripcion: string,
    imgUrl: string
  ) {
    this.nombre = nombre;
    this.precio = precio;
    this.descripcion = descripcion;
    this.imgUrl = imgUrl;
  }
}
