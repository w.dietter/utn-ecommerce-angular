import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CatalogoComponent } from './pages/catalogo/catalogo.component';
import { RegistroComponent } from './pages/usuarios/registro/registro.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { AltaProductoComponent } from './pages/admin/altaproducto/altaproducto.component';
import { AdminComponent } from './pages/admin/admin.component';
import { UsuariosComponent } from './pages/usuarios/usuarios/usuarios.component';

const routes: Routes = [
  {path:"", component:HomeComponent},
  {path:"catalogo", component:CatalogoComponent},
  {path:"catalogo/:id", component:ProductoComponent},
  {path:"registro", component:RegistroComponent},
  {path:"registro/usuarios", component: UsuariosComponent},
  {path:"admin", component:AdminComponent},
  {path:"admin/altaproducto", component: AltaProductoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
